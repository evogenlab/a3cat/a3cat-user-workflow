# Define headers in the assembly config file
ASSEMBLY_NAME = 'Assembly Name'
FILE_PATH = 'File Path'
SPECIES_TAXID = 'Species TaxId'
LINEAGE_DATASETS = 'BUSCO Lineage Datasets'
COMPARISON_LEVELS = 'Comparison Levels'
TAXONOMY_FILE = 'Taxonomy File'
