import logging
import os
from ete3 import NCBITaxa
from utils import setup_logging, save_dict_to_json

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    db_file_path = snakemake.input[0]
    taxonomy_file_path = snakemake.output[0]
    assembly_info = snakemake.params.assembly_info
    taxid_header = snakemake.params.taxid_header
    taxonomy_header = snakemake.params.taxonomy_header
    try:
        species_taxid = assembly_info[taxid_header][0]
    except KeyError:
        species_taxid = None
    try:
        manual_taxonomy = assembly_info[taxonomy_header][0]
    except KeyError:
        manual_taxonomy = None

    print(species_taxid)

    if species_taxid is None:
        logging.info('Manual taxonomy provided, checking taxonomy')
        levels = []
        for level_name, level_value in manual_taxonomy.items():
            if level_value is not None:
                levels.append(level_name)
        if len(levels) == 0:
            error_msg = ('No species TaxID was provided but the manual taxonomy was empty.'
                         'Please provide an NCBI TaxID or specify at least one taxonomical '
                         'level manually in the config file (config/config.yaml)')
            raise ValueError(error_msg)
        taxonomy = manual_taxonomy
    else:
        logging.info(f'Retrieving information from NCBI Taxonomy database for {species_taxid}')
        logging.info('Loading ete3 database')
        ncbi = NCBITaxa(dbfile=db_file_path)
        lineage = ncbi.get_lineage(species_taxid)
        lineage_names = ncbi.get_taxid_translator(lineage)
        lineage_ranks = ncbi.get_rank(lineage)
        taxonomy = {rank: lineage_names[taxid] for taxid, rank in lineage_ranks.items()}

    logging.info(f'Saving taxonomy info to <{taxonomy_file_path}>')
    save_dict_to_json(taxonomy_file_path, taxonomy)
    logging.info(f'Successfully retrieved taxonomy information for the sequenced species')
