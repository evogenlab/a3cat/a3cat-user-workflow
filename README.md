# A3Cat user workflow

This workflows compute quality metrics on user-provided assemblies and compares them with metrics for assemblies in the same taxonomic clade from the A3Cat. 

## Setup

### Requirements

This pipeline requires Snakemake to be installed and configured properly. To setup Snakemake and conda the recommended way, follow [these instructions](#recommended-setup-for-snakemake-and-conda).

### Installing the workflow

Clone the git repository:

*Clone with ssh if you have it setup*
```bash

git clone git@gitlab.com:evogenlab/a3cat-user-workflow.git
```

*Clone with https if you don't have ssh setup properly*
```bash

git clone https://gitlab.com/evogenlab/a3cat-user-workflow.git
```

## Running the workflow

### Configure the run

Configuration is done using the file `config.yaml`. This file is thoroughly commented to simplify the configuration process.

### Run the workflow

After configuration, run snakemake from the main workflow directory:

```bash
snakemake --use-conda [ -j <n_cores> ] [ --profile <profile> ]
```

### Workflow output

...



## Recommended setup for Snakemake and Conda

### Installing Miniconda

Detailed instructions [here](https://docs.conda.io/en/latest/miniconda.html). Make sure to install the Python3 version and have conda setup in your PATH.

### Installing Snakemake

Detailed instructions are available on the [Snakemake documentation](https://snakemake.readthedocs.io/en/latest/getting_started/installation.html).

1. Create a new conda environment (recommended): `conda create -n snakemake`

2. Activate this environment: `conda activate snakemake`

3. Install mamba: `conda install -c conda-forge mamba`

4. Install snakemake: `mamba install -c bioconda -c conda-forge snakemake`

5. Test your snakemake install: `snakemake --version`

With this setup, you just have to activate the `snakemake` conda environment before using snakemake with `conda activate snakemake`.
